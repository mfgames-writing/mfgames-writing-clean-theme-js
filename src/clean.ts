import * as liquid from "@mfgames-writing/liquid";
import path = require("path");
import { ContentArgs, Theme, ContentTheme } from "@mfgames-writing/contracts";

function loadCleanTheme()
{
    var theme = new CleanTheme();
    return theme;
}

export class CleanTheme extends liquid.LiquidTheme
{
    constructor()
    {
        let templateDirectory = path.join(__dirname, "..", "templates");
        let styleDirectory = path.join(__dirname, "..", "styles");
        super(templateDirectory, styleDirectory);
    }

    public getContentTheme(content: ContentArgs): ContentTheme
    {
        // Start with the defaults.
        var results: any = super.getContentTheme(content);

        // Figure out the defaults for types.
        switch (content.element)
        {
            case "legal":
            case "title":
                results.weasyprint = {
                    pad: "none",
                };
        }

        // Return the theme elements.
        return results;
    }
}

export default loadCleanTheme;
